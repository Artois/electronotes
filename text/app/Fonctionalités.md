Dans cette version de l'application la majorité des fonctionnalités de l'application sont implementées en utilisant Electron. 
Plusieurs fois la mention `CtrlOrCmd+Lettre` apparait dans cette note pour définir les raccourcis clavier. 
Quand ce mot est présent cela signifie que le raccourci sur Linux et Windows utilise la touche Ctrl plus une lettre 
alors que sur Mac on utilisera la touche Cmd en plus de la lettre.

#### Menu d'application

Electronotes possède un menu d'application (souvent situé en haut) qui possède 4 sous-menus :

- **Fichier** : Gestion des notes
    - ***Nouveau*** *(CtrlOrCmd+N)* : Ouvre la fenêtre de création d'une nouvelle note
    - ***Importer*** *(CtrlOrCmd+I)* : Importe une note à partir d'un fichier. Plus d'informations dans `Export / Import`
    - ***Reset*** *(CtrlOrCmd+R)* : Remet l'application dans son état d'origine avant toute modification
- **Démo** : Démonstration des posibilités d'Electron
    - ***Calcul nombre premier*** : Permet de calculer si un nombre donné est premier. 
    Le but est de montrer qu'Electron est capable de faire des calculs en arrière plan sans impacter le reste de l'application. 
    Pour que le calcul ne soit pas trop rapide il faut renseigner un nombre aux alentours des 10 milliards
    - ***Reduire l'application*** : Ferme la fenêtre et met l'application dans la barre des tâches. 
    Pour réouvrir l'application il suffit de cliquer sur l'icone
- **Fenêtre** : Gestion de la fenêtre
    - ***Pleine écran*** *(F11)*
    - ***Minimiser*** *(CtrlOrCmd+M)*
    - ***Fermer*** *(CtrlOrCmd+W)*
    - ***Outils Développeur*** *(CtrlOrCmd+Maj+I)* : Ouvre la console de développement de Chromium
- **À propos** : Infos et version de l'application
    - ***Electron*** : Ouvre le site d'Electron dans le navigateur
    - ***Electronotes*** : Ouvre le git d'Electronotes (cette application) dans le navigateur
    - ***Versions*** : Indique les versions d'Electron, de Node et de Chromium utilisées par l'application

#### Menu contextuel

Le menu contextuel s'affiche en faisant clic droit dans l'application. Il propose plusieurs actions :

- **Nouveau** : Ouvre la fenêtre de création d'une nouvelle note
- **Importer** : Importe une note à partir d'un fichier. Plus d'informations dans `Export / Import`
- **Exporter en markdown** : Si coché les fichiers exportés seront en markdown et non en JSON. Plus d'informations dans `Export / Import`
- **Afficher chargement** : Affiche/Cache l'écran de chargement
- **Outils Développeur** : Ouvre la console de développement de Chromium

#### Export / Import

Il est possible d'importer et d'exporter des notes sous deux formats, en markdown et en JSON.
Quand l'export est fait en markdown le titre de la note devient le nom du fichier et son contenu devient celui du fichier.
Dans le cas de l'export en JSON un fichier avec le format ci-dessous sera généré :

```json
{
    "id": "id unique de la note dans l'application",
    "title": "titre de la note",
    "content": "contenu de la note en markdown"
}
```

C'est ce type de fichier qui est utilisé pour importer les notes dans l'application (les fichiers markdown importés sont transformés dans ce format).
C'est aussi sous cette forme que sont stockées les données dans l'application.

Le choix du type d'export se fait dans le menu contextuel, par défaut l'export se fait en markdown. Pour exporter une note il suffit de faire un cliquer-déposer d'une note de l'application vers un gestionnaire de fichier.

---

Il est possible d'importer les deux types de fichiers de l'export. Pour importer il suffit de sélectionner l'option dans le menu d'application (Fichier > Importer), dans le menu contextuel ou de faire le raccourci CtrlOrCmd+I.

Lors de l'import d'un fichier en markdown le nom du fichier devient le titre de la note et le contenu du fichier devient celui de la note.
Un id est généré automatiquement lors de la création du JSON correspondant à la note pour le sotckage.

Pour l'import d'un fichier JSON celui-ci doit respecter la structure décrite au-dessus. 
De plus l'id ne doit pas être déjà utilisé par une autre note, sinon un message d'erreur est retourné.
Le JSON est utilisé tel quel pour stocker et afficher la note.