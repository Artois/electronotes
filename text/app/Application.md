Cette application est écrite entièrement avec des langages web (HTML / CSS / JS) et se base sur node.js. Cette version de l'application ne fonctionne pas dans les navigateurs

#### Outils

Plusieurs outils ont été installés avec l'application pour pouvoir l'utiliser simplement et sans installation de dépendances externes :

- [Electron forge](https://www.electronforge.io) : Outils pour créer, publier et installer facilement des applications Electron
- [Commander](https://github.com/tj/commander.js) : Gestion CLI pour node (permet de gérer les arguments pour savoir quelle version de l'application lancer)
- [node-sass](https://github.com/sass/node-sass) : Compilateur fichier SCSS en CSS
- [del](https://github.com/sindresorhus/del) : Suppression de fichier

#### Frameworks

L'application utilise plusieurs frameworks pour faciliter le développement et la mise en page de l'application :

- [Materialize](https://materializecss.com) : Framework CSS / JS utilisé pour le design et les fonctionnalités graphiques du site (pop-up, toast, chargement, ...)
- [Jquery](https://jquery.com) : Permet de simplifier l'interaction avec le DOM
- [Google code prettify](https://github.com/googlearchive/code-prettify) : Met en forme (couleur, police, ...) les extraits de code présents dans la page
- [Marked](https://marked.js.org/) : Compilateur markdown en HTML
- [DOMPurify](https://github.com/cure53/DOMPurify) : Nettoie l'HTML pour éviter les injections XSS
- [SimpleMDE](https://simplemde.com) : Éditeur markdown WYSIWYG

Toute la partie métier de l'application a été écrite en javascript sans framework particulier. Pour les fichiers de style le format SCSS a été choisi et un compilateur SCSS en CSS est intégré au projet.

#### Lancement

Cette application ne peut être lancée uniquement qu'avec Electron, il est **impossible** de lancer l'application autrement.
En effet l'application utilise des fonctionnalités propre à Electron (et node.js) et ne peut donc être lancée que dans cet environnement.

Pour lancer l'application il suffit d'utiliser l'une des commandes suivantes

```bash
npm start

npm run app

npm run app:electron
```

Au lancement de l'application les fichiers SCSS sont recompilés en CSS. 
De même lors de la fermeture de l'application si des fichiers temporaires on été créés, ils sont supprimés.

#### Compilation

Il est théoriquement possible de compiler ce projet pour avoir un exécutable de l'application. Cependant la compilation n'a jamais été testée.
De plus la structure du projet est prévue pour montrer l'intérêt et les possibilités d'Electron et n'est pas faite pour être compilée et déployée sous forme d'exécutable.
La compilation est gérée par `Electron forge` et peut être lancée avec la commande suivante :

```bash
npm run make
```

La configuration pour la création des éxecutables pour les différents OS est dans le fichier `package.json` dans `config.maker`. 
La configuration actuelle est celle par défaut d'Electron forge. 
Plus d'informations sur les différents éxecutables qu'il est possible de générer et les conditions pour pouvoir compiler le projet sur les différents OS sont disponibles sur [le site web d'Electron forge](https://www.electronforge.io/config/makers).

#### Source

Les sources de cette application sont disponibles dans le dossier `app/` à la racine du projet. Ce dossier possède 3 sous-dossiers :

- Le dossier `data/` qui contient les données des notes présentes par défaut
- Le dossier `main/` qui contient le code source du processus principal d'Electron (plus d'informations dans la note Electron). 
Les fichiers dans le dossier sont les fichiers de code éxecutés par Electron, le dossier `function/` regroupe les fonctions utilitaires pour ces fichiers et
le dossier `asset/` contient les icones utilisés par l'application.
- Le dossier `renderer/` qui contient les fichiers pour le rendu de l'application. C'est dans ce dossier que se trouve la base de code commune entre les deux versions de l'application.
Les fichiers HTML à la racine du dossier sont les pages qui sont chargées par Electron au lancement. 
Le dossier `common/` regroupe tous les fichiers Javascript et CSS communs à toutes les pages de l'application.
Le dossier `page/` contient toutes les pages de l'application, chaque page possède son propre dossier avec ces fichiers HTML et Javascript.
Enfin dans le dossier `service/` se trouve des class utilitaires pour gérer par exemple le stockage ou le routage entre les pages.

De plus le dossier `helper/` regroupe des fichiers de fonctions utilitaires à thème (par exemple la gestion des fichiers).
Ces fonctions sont utilisées dans toute l'application (y compris dans le fichier `main.js` qui est le fichier principal de l'application).
Pour finir le dossier `data/` sert à stoquer les infos du dernier lancement (quelle version de l'application a été utilisée, pour reset les données en cas de changement, 
ainsi que les fichiers temporaires