Ceci est une note. Elle contient des informations diverses. Cette note explique les fonctionnalités de base de l'application

#### Ajouter une note

Il est possible d'ajouter autant de notes que vous voulez.
Pour ajouter une note il suffit d'appuyer sur le bouton + ou de faire Ctrl+N, une nouvelle fenêtre s'ouvre pour créer la note. 
Chaque note apparaitra sous forme de carte dans la page d'accueil. 
Cliquez dessus pour voir ce qu'elles contiennent. 
La création d'une note utilise un éditeur WYSIWYG markdown pour mettre en forme le texte (avec des titres, du code, ...).

#### Modifier une note

Il est aussi possible de modifier une note, pour ce faire cliquez simplement sur le bouton modifier sur la carte ou sur la note lorsqu'elle est affichée. 
La modification ouvre la page d'édition dans une nouvelle fenetre
Le système d'édition est le même que pour l'ajout et utilise le markdown pour mettre en forme le texte.

#### Supprimer une note

Pour supprimer une note il suffit de cliquer sur le bouton supprimer sur la carte ou dans la note. 
Une demande de confirmation apparait pour valider ou non la suppression.
Toute note est définitivement supprimée et ne peut être récupérée.

#### Modifier le nom d'utilisateur

Il est possible de modifier son nom d'utilisateur. Pour ça il suffit de modifier le champ texte à côté de "Bonjour".
Le nom est alors modifié en direct et sauvegardé.

#### Infos sur l'application

Cette version de l'application utilise des fonctionnalités propres à Electron et ne peut pas être utilisée dans un navigateur (via un serveur HTTP).
Les fonctionnalités utilisant Electron pour être implementées dans cette version de l'application sont détaillées dans la note `Fonctionnalités`.
De même pour avoir plus d'informations sur les technologies, la structure et les fichiers du projet réferrez vous à la note `Application`.
Enfin pour avoir des informations sur le fonctionnement d'Electron dans cette application réferrez vous à la note `Electron`.
