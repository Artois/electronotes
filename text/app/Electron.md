Electron permet de faire des applications avec les technologies web (HTML 5 / CSS 3 / Javascript). 
Les applications créées avec Electron peuvent être compilées pour tous les OS existants, ce qui permet avec une seule base de code d'avoir un executable pour tous les environements. 
De plus les applications Electron utilisent les éléments d'interfaces de l'OS cible, de ce fait l'application ressemblera à une application native de l'OS.

#### Principe

Electron utilise Node.js et Chromium pour permettre de faire des applications. 
Chromium est utilisé pour le rendu des fichiers HTML et Node.js pour exécuter le code Javascript du processus main (Node est basé sur la version 8 du moteur javascript de Chromium).
Pour les processus de rendus il est possible (via une option lors de la création de la fenêtre) d'utiliser le moteur javascript de Node ou celui intégré à Chromium.
Si l'on utilise le moteur de Chromium dans le rendu il n'est pas possible de faire plus que dans un navigateur classique.

Dans cette version de l'application le moteur de Node est utilisé partout, mais dans la version sans intégration spécifique d'Electron c'est le moteur de Chromium qui est utilisé.

#### Fonctionnement

Electron possède deux types de processus, le processus principal et les processus de rendus.
Le processus principal est celui du fichier `main.js` chargé lors du lancement de l'application, 
les processus de rendus correspondent chacun à une fenêtre de l'application.
Les fenêtres peuvent être visibles ou non (ainsi le calcul du nombre premier est fait dans un processus de rendu lié à une fenêtre invisible).

Les deux types de processus peuvent communiquer entre eux par le biais d'évènements et d'écouteurs sur ces derniers.
Chacun possède des fonctionnalités qui lui sont propre et que l'autre ne peut pas réaliser 
(par exemple les notifications ne peuvent être émises que depuis un processus de rendu).

Au lancement de l'application le fichier `main.js` est éxécuté. Dans un premier temps il va charger les autres fichiers utilisés dans le processus principal.
Ces fichiers servent par exemple à gérer les différents menus de l'application.
Ensuite l'application va charger la page HTML correspondant au dashboard et ouvrir une fenêtre (et donc un processus de rendu).
Dans ce processus de rendu tous les imports javascript vont être chargés par les balises script dans le fichier HTML.
Le fichier principal pour le chargement des dépendances et des fonctions necessaires pour les processus de rendus est `electronotes.js`