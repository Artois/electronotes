Cette application est écrite entièrement avec des langages web (HTML / CSS / JS) et fonctionne avec n'importe quel navigateur moderne.

#### Outils

Plusieurs outils ont été installés avec l'application pour pouvoir l'utiliser simplement et sans installation de dépendances externes :

- [Electron forge](https://www.electronforge.io) : Outils pour créer, publier et installer facilement des applications Electron
- [Commander](https://github.com/tj/commander.js) : Gestion CLI pour node (permet de gérer les arguments pour savoir quelle version de l'application lancer)
- [node-sass](https://github.com/sass/node-sass) : Compilateur fichier SCSS en CSS
- [http-server](https://github.com/http-party/http-server) : Server HTTP en node.js

#### Frameworks

L'application utilise plusieurs frameworks pour faciliter le développement et la mise en page :

- [Materialize](https://materializecss.com) : Framework CSS / JS utilisé pour le design et les fonctionnalités graphiques du site (pop-up, toast, chargement, ...)
- [Jquery](https://jquery.com) : Permet de simplifier l'interaction avec le DOM
- [Google code prettify](https://github.com/googlearchive/code-prettify) : Met en forme (couleur, police, ...) les extraits de code présents dans la page
- [Marked](https://marked.js.org/) : Compilateur markdown en HTML
- [DOMPurify](https://github.com/cure53/DOMPurify) : Nettoie l'HTML pour éviter les injections XSS
- [SimpleMDE](https://simplemde.com) : Éditeur markdown WYSIWYG

Toute la partie métier de l'application a été écrite en javascript sans framework particulier. Pour les fichiers de style le format SCSS a été choisi et un compilateur SCSS en CSS est intégré au projet.

#### Lancement

Cette application peut soit être lancée dans un navigateur via un serveur HTTP (un serveur est mis à disposition avec l'application), soit directement dans electron. 
Il est ***impossible*** de lancer l'application directement depuis le navigateur de fichier en ouvrant le fichier index.html dans le navigateur. 
En effet l'application fait des requêtes GET pour charger les notes présentes par défaut, or la sécurité d'un navigateur bloque ces requêtes lors de l'exécution depuis le système de fichier.

Pour lancer l'application dans un serveur web il faut utiliser la commande suivante (le navigateur s'ouvre automatiquement sur l'URL de l'application) :

```bash
npm run src:web
```

Pour lancer l'application dans electron il faut utiliser la commande suivante (plus d'info sur electron dans la note dédiée) :

```bash
npm run src:electron
```

La compilation des fichiers SCSS en CSS se fait automatiquement lors de l'installation de l'application avec `npm i`.
Il est possible de recompiler les fichiers en utilisant la commande suivante :

```bash
npm run prepare
```

#### Source

Que l'application soit lancée sur un serveur web pour être affichée dans un navigateur ou directement dans electron, ce sont les même fichiers sources qui sont utilisés. 
Les fichiers sources de cette version de l'application sont disponibles dans le dossier `src/` à la racine du projet.

Le code javascript propre à l'application se trouve directement dans les fichiers HTML. 
Le dossier `css/` contient toutes les feuilles de style de l'application (format scss et css) dont le fichier `style.scss` qui a été écrit pour l'application. 
Le dossier `js/` contient les fichiers avec les fonctions javascript utilisées. 
Le fichier `binder.js` (écrit pour le projet) contient des fonctions pour automatiser l'affichage de données dans le document HTML et le fichier `script.js` contient des fonctions utilitaires pour l'application. 
Enfin le dossier `data/` contient les données des notes présentes par défaut.

De plus le dossier `helper/` regroupe des fichiers de fonctions utilitaires sur un thème (par exemple la gestion des fichiers).
Ces fonctions sont utilisées uniquement dans le  fichier `main.js` (qui est le fichier principal de l'application) dans cette version.
Pour finir le dossier `data/` sert à stoquer les infos du dernier lancement (quelle version de l'application a été utilisée, pour reset les données en cas de changement).