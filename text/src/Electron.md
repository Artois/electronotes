Electron permet de faire des applications avec les technologies web (HTML 5 / CSS 3 / Javascript). 
Les applications créées avec Electron peuvent être compilées pour tous les OS existants, ce qui permet avec une seule base de code d'avoir un executable pour tous les environements.
De plus les applications Electron utilisent les éléments d'interfaces de l'OS cible, de ce fait l'application ressemblera à une application native de l'OS.

#### Source

Les sources de cette application sont exactement les mêmes que lorsque l'application est lancée sur un serveur HTTP. 
Les fichiers utilisés sont exactement les mêmes. Pour plus d'informations sur les dossiers sources, se réferrer à la partie `Source` de la note application.

#### Principe

Electron utilise Node.js et Chromium pour permettre de faire des applications. 
Chromium est utilisé pour le rendu des fichiers HTML et Node.js pour éxécuter le code Javascript (Node est basé la version 8 du moteur javascript de Chromium).

#### Fonctionnement

Lors du lancement de l'application Electron le fichier `main.js` est appelé. Ce fichier permet de lancer l'application avec ou sans l'intégration d'Electron. 
Dans le cas de cette application (sans intégration d'Electron) l'application va seulement charger la page `index.html` 
dans le dossier `src/` (la création de la fenêtre est gérée par des fonctions communes aux deux versions). 
Lorsque toutes les fenêtres sont fermées l'application s'arrête sauf sur Mac où il faut utiliser Cmd+Q (fonctionnement par defaut d'Electron sur Mac).

#### Menu

Cette version de l'application étant identique à la version web, elle n'a aucun code spécifique à Electron. 
De ce fait aucune indication n'est donnée pour générer les menus. En l'abscence d'indications Electron genère uniquement un menu d'application (en haut de l'application) par défaut et aucun menu contextuel (clic droit). 
Le menu est anglais et contient des raccourcis souvent utilisés comme copier/coller, agrandir ou minimiser la fenêtre, ...