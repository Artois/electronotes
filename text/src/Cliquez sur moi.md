Ceci est une note. Elle contient des informations diverses. Cette note explique les fonctionnalités de l'application

#### Ajouter une note

Il est possible d'ajouter autant de notes que vous voulez. 
Pour ajouter une note il suffit d'appuyer sur le bouton +
Chaque note apparaitra sous forme de carte dans la page d'accueil. 
Cliquez dessus pour voir ce qu'elles contiennent. 
La création d'une note utilise un éditeur WYSIWYG markdown pour mettre en forme le texte (avec des titres, du code, ...).

#### Modifier une note

Il est aussi possible de modifier une note, pour ce faire cliquez simplement sur le bouton modifier sur la carte ou sur la note lorsqu'elle est affichée. 
Le système d'édition est le même que pour l'ajout et utilise le markdown pour mettre en forme le texte.

#### Supprimer une note

Pour supprimer une note il suffit de cliquer sur le bouton supprimer sur la carte ou dans la note. 
Attention il n'y a aucune demande de confirmation. 
Toute note est définitivement supprimée et ne peut être récupérée.

#### Modifier le nom d'utilisateur

Il est possible de modifier son nom d'utilisateur. Pour ça il suffit de modifier le champ texte à côté de "Bonjour".
Le nom est alors modifié en direct et sauvegardé.

#### Menu

Le menu en haut à droite de la page contient des liens vers le site d'electron et vers le git du projet. 
Le bouton reset permet de remettre à zero l'application. 
Remettre à zero l'application supprimera toutes les notes ajoutées par l'utilisateur et remettra les notes par défaut dans leur état d'origine.

#### Infos sur l'application

Cette version de l'application n'utilise pas les fonctionnalités propres à Electron et peut être utilisée dans un navigateur (via un serveur HTTP).
Pour plus d'informations sur les technologies, la structure et les fichiers du projet réferrez vous à la note `Application`.
De même plus d'informations sur le fonctionnement d'Electron dans cette application sont disponibles dans la note `Electron`.