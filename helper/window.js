const config = require('../config');
const { BrowserWindow } = require('electron');
const path = require('path');
const window = {};

window.hidden = function (file) {
    const win = new BrowserWindow({
        width: 0,
        height: 0,
        show: false,
        webPreferences: {
            nodeIntegration: true
        }
    });
    if (file) {
        win.loadFile(file);
    }
    return win;
}

window.new = function (file, width, height) {
    // Default values
    width = width ? width : config.window.width;
    height = height ? height : config.window.height;
    // New window
    const win = new BrowserWindow ({
        width: width,
        height: height,
        icon: path.join(__dirname, '../electronotes.png'),
        webPreferences: {
            nodeIntegration: true
        }
    });
    win.loadFile(file);
    return win;
}

window.simple = function (file, width, height) {
    // Default values
    width = width ? width : config.window.width;
    height = height ? height : config.window.height;
    // New window
    const win = new BrowserWindow ({
        width: width,
        height: height,
        icon: path.join(__dirname, '../electronotes.png'),
        webPreferences: {
            nodeIntegration: false
        }
    });
    win.loadFile(file);
    return win;
}

window.frameless = function (file, parent, width, height) {
    // Default values
    width = width ? width : config.window.width;
    height = height ? height : config.window.height;
    // Options
    const opts = {
        width: width,
        height: height,
        frame: false,
        icon: path.join(__dirname, '../electronotes.png'),
        webPreferences: {
            nodeIntegration: true
        }
    }
    // Add parent if have one
    if (parent) {
        opts.parent = parent;
    }
    // New window
    const win = new BrowserWindow (opts);
    win.loadFile(file);
    return win;
}

module.exports = window;