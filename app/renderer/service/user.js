class UserService {

    get username() {
        if (!localStorage.username) {
            localStorage.setItem('username', 'Utilisateur')
        }
        return localStorage.username;
    }

    set username(newName) {
        if (newName.trim() === '') {
            newName = 'Utilisateur';
        }
        localStorage.setItem('username', newName);
    }

}

module.exports = new UserService();