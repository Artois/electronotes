ipcRenderer.on('reset-app', (event, arg) => {
    loader();
    DataService.reset();
    RouterService.reload();
});

ipcRenderer.on('app-loader', (event, arg) => {
    loader();
});

ipcRenderer.on('import-card', (event, card) => {
    console.log(card);
    // If card with same id already exist
    if (localStorage.getItem(card.id)) {
        ipcRenderer.send('card-already-exist', card.id);
    } else {
        // Add card
        DataService.add(card.id, card.title, card.content);
    }
});