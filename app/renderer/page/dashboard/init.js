const init = {
    floatingButton: null,
    modal: null,
    tooltip: null,
    scope: {}
};

init.exec = function (print) {
    // Load floating button
    init.floatingButton = M.FloatingActionButton.init(document.querySelectorAll('.fixed-action-btn'))[0];
    // Load modal
    init.modal = M.Modal.init(document.querySelectorAll('.modal'))[0];
    // Load tooltip
    init.tooltip = M.Tooltip.init(document.querySelectorAll('.tooltipped'))[0];
    // Load data
    init.scope = binder({
        card: DataService.data
    });
    init.scope.username = UserService.username;
    // Set the link to edit
    $('#float-add-btn').attr('href', RouterService.path('edit'));
    // Print data
    print(init.scope);
    // Hide loader
    loader();  
    //Show discovery the first time
    if (!localStorage.discovery) {
        // Load discovery
        const discovery = M.TapTarget.init(document.querySelectorAll('.tap-target'), {onClose: () => {
            localStorage.setItem('discovery', true)
        }})[0];
        setTimeout(() => {discovery.open();}, 500);
    } 
}

module.exports = init;