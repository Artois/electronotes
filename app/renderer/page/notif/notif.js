// Wait notification demand
ipcRenderer.once('new-notif', (event, notification) => {
    // Check notification data
    if (notification.title && notification.body && notification.icon) {
        // Create notification
        const notif = new window.Notification(notification.title, notification); 
        // Close notif after 2.5 sec
        setTimeout(() => {
            notif.close();
            ipcRenderer.send('notif-close');
        }, 2500)   
    } else {
        ipcRenderer.send('notif-close');
    }
});