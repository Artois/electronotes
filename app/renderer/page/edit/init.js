const init = {
    tooltip: null,
    actionBtn: null,
    mdEditor: null,
    scope: {}
};

init.exec = function (loadMdEditor, GET) {
    // Load tooltip
    init.tooltip = M.Tooltip.init(document.querySelectorAll('.tooltipped'))[0];
    // Load action buttons
    init.actionBtn = M.FloatingActionButton.init(document.querySelectorAll('.fixed-action-btn'), {direction: 'left'});
    // Load markdown editor
    init.mdEditor = loadMdEditor();
    // Load username
    init.scope = binder({
        username: UserService.username
    });
    // Check mode
    if (GET.id) {
        // Update
        document.title = 'Electronotes - Modification';
        // Load data
        const data = DataService.getById(GET.id);
        $('#title').val(data.title);
        init.mdEditor.value(data.content);
        M.updateTextFields();
    } else {
        // Create
        document.title = 'Electronotes - Création';
        //Disabled remove button
        $('#remove-btn').attr('disabled', true);
    }
}

module.exports = init;