// Wait event to calc prime number
ipcRenderer.once('calc-prime-number', (event, number) => {
    const result = primalite(number);
    ipcRenderer.send('prime-number-result', result);
});

function primalite (nombre) {
    // Calcul de la racine carré du nombre a tester
    const sqrt = Math.ceil(Math.sqrt(nombre));
    // Recupération des nombres premiers inferieur à la racine
    const primeNumber = eratosthene(sqrt);
    // Test si le nombre est divisible par les nombres premiers
    for(const prime of primeNumber) {
        if (nombre % prime === 0) {
            // Le nombre est divisible et donc pas premier
            return false;
        }
    }
    // Si on arrive ici le nombre n'est pas divisible et donc premier
    return true;
}

function eratosthene (nombre) {
    const retour = [];
    const valeur = [];
    const marqueur = [];
    let compteur = 0;
    let mem = 0
    // Initilisation des tableaux
    for(let i = 2; i <= nombre; i++) {
        valeur[i - 2] = i;
        marqueur[i - 2] = 0;
    }
    // Parcours et elimination des nombres
    for (let i = 0; i <= nombre - 2; i++) {
        // Regarde si le nombre n'est pas déjà marqué
        if (marqueur[i] === 0) {
            // On marque tous les nombres multiple de ce nombre
            for(j = i + 1; j <= nombre - 2; j++) {
                if (valeur[j] % valeur[i] == 0) {
                    marqueur[j] = 1;
                    break;
                }
            }
        }
        // Augmentation compteur
        compteur++;
    }
    // Creation du resultat
    for(let i = 0; i < compteur; i++) {
        // Recuperation des nombres non marqué
        for(j = mem; j < nombre - 2; j++) {
            if (marqueur[j] === 0) {
                // Ajout du nombre
                retour[i] = valeur[j];
                // Pour ne pas le reprendre
                marqueur[j] = 2;
                mem = j;
                // Pour s'arreter
                break;
            }
        }
    }
    // Retour
    return retour;
}