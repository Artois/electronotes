let answerKey = '';

ipcRenderer.once('dialog-key', (event, key) => {
    answerKey = key;
})

function closeWindow() {
    ipcRenderer.send('dialog-answer' + answerKey, false);
    remote.getCurrentWindow().close();
}

function validate() {
    if(!$('#number').val()) {
        M.toast({
            html: 'Le nombre est invalide',
            classes: 'red'
        });
    } else if (('' + parseInt($('#number').val())) !== $('#number').val()) {
        M.toast({
            html: 'Le nombre n\'est pas un entier',
            classes: 'red'
        });
    } else if (parseInt($('#number').val()) < 1) {
        M.toast({
            html: 'Le nombre doit être un entier positif',
            classes: 'red'
        });
    } else {
        send(parseInt($('#number').val()));
    }
}

function send(number) {
    ipcRenderer.send('dialog-answer' + answerKey, number);
}