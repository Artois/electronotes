const {Menu, shell} = require('electron');
const menu = require('./function/menu');
const dialog = require('./function/dialog');
const demo = require('./function/demo-application-menu');

let template = [
  {
    label: 'Fichier',
    submenu: [
      {
        label: 'Nouveau',
        accelerator: 'CmdOrCtrl+N',
        click: menu.new
      },
      {
        label: 'Importer',
        accelerator: 'CmdOrCtrl+I',
        click: menu.import
      },
      {
        type: 'separator'
      },
      {
        label: 'Reset',
        accelerator: 'CmdOrCtrl+R',
        click: () => {
          mainWindow.webContents.send('reset-app');
        }
      }
    ]
  },
  {
    label: 'Démo',
    submenu: [
      {
        label: 'Calcul nombre premier',
        click: demo.primeNumber
      },
      {
        label: 'Reduire l\'application',
        click: demo.tray
      }
    ]
  },
  {
    label: 'Fenêtre',
    submenu: [
      {
        label: 'Pleine écran',
        accelerator: (() => {
          if (process.platform === 'darwin') {
            return 'Ctrl+Command+F'
          } else {
            return 'F11'
          }
        })(),
        click: (item, focusedWindow) => {
          if (focusedWindow) {
            focusedWindow.setFullScreen(!focusedWindow.isFullScreen())
          }
        }
      },
      {
        label: 'Minimiser',
        accelerator: 'CmdOrCtrl+M',
        role: 'minimize'
      }, 
      {
        label: 'Fermer',
        accelerator: 'CmdOrCtrl+W',
        role: 'close'
      },
      {
        type: 'separator'
      },
      {
        label: 'Outils Developpeur',
        accelerator: (() => {
          if (process.platform === 'darwin') {
            return 'Alt+Command+I'
          } else {
            return 'Ctrl+Shift+I'
          }
        })(),
        click: menu.devTool
      }
    ]
  },
  {
    label: 'À propos',
    submenu: [
      {
        label: 'Electron',
        click: () => {
          shell.openExternal('https://www.electronjs.org');
        }
      }, 
      {
        label: 'Electronotes',
        click: () => {
          shell.openExternal('https://gitlab.univ-artois.fr/arthur_brandao/conf-electron');
        }
      },
      {
        label: 'Versions',
        click: () => {
          dialog.message(
            'Versions',
            `Electron : ${process.versions.electron}\nChrome : ${process.versions.chrome}\nNode : ${process.versions.node}`,
            'info',
            ['Fermer']
          );
        }
      }
    ]
  }
];

const appMenu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(appMenu);