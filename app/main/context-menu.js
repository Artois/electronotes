const { Menu, MenuItem, app } = require('electron');
const config = require('../../config');
const func = require('./function/menu');

const menu = new Menu();
menu.append(new MenuItem({
    label: 'Nouveau',
    click: func.new
}));
menu.append(new MenuItem({
    label: 'Importer',
    click: func.import
}));
menu.append(new MenuItem({type: 'separator'}));
menu.append(new MenuItem({
    label: 'Exporter en markdown',
    type: 'checkbox',
    checked: exportToMd,
    click: () => {
        exportToMd = !exportToMd;
    }
}));
menu.append(new MenuItem({
    label: 'Afficher chargement',
    type: 'checkbox',
    checked: false,
    click: () => {
        mainWindow.webContents.send('app-loader');
    }
}));
menu.append(new MenuItem({type: 'separator'}));
menu.append(new MenuItem({
    label: 'Outils Developpeur',
    click: func.devTool
}))

app.on('browser-window-created', (event, win) => {
    win.webContents.on('context-menu', (evt, params) => {
        menu.popup(win, params.x, params.y);
    });
});