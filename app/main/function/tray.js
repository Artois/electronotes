const {app, Tray, Menu} = require('electron');
const path = require('path');
const window = require('../../../helper/window');
const tray = {};

tray.openApp = function () {
    if (appIcon) {
        // Set tray inactive
        trayInactive = true;
        // Remove tray
        appIcon.destroy();
        // Re create the main window on dashboard page and close the invisible window
        mainWindow = window.new(path.join(__dirname, '../../renderer/page/dashboard/dashboard.html'));
    }
};

tray.closeApp = function () {
    if (appIcon) {
        appIcon.destroy();
        app.quit();
    }
}

tray.active = function () {
    // Set tray active
    trayInactive = false;
    // Create tray
    appIcon = new Tray(path.join(__dirname, '../asset/tray.png'));
    appIcon.setToolTip('Electronotes');
    appIcon.setContextMenu(menu);
    // Hide app
    mainWindow.close();
    mainWindow = null;
    // Left click => open app
    appIcon.on('click', (event) => {
        tray.openApp();
    });
};

module.exports = tray;

/* --- Var and event --- */

let appIcon = null;
app.on('quit', () => {
    if (appIcon) {
        appIcon.destroy();
    }
});

const template = [
    {
        label: 'Ouvrir',
        click: tray.openApp,
    },
    {
        label: 'Quitter',
        click: tray.closeApp
    }
];
const menu = Menu.buildFromTemplate(template);