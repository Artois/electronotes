const {ipcMain} = require('electron');
const path = require('path');
const window = require('../../../helper/window');
const notif = {};

let hiddenWindow = null;

notif.create = function (title, content) {
    // If a notification is already displayed
    if (hiddenWindow) {
        return;
    }
    // Notification data
    const data = {
        title: title,
        body: content,
        icon: path.join(__dirname, '../asset/icon.png')
    }
    // Wait close information
    ipcMain.once('notif-close', (event, arg) => {
        hiddenWindow.close();
        hiddenWindow = null;
    })
    // Create new invisible window to show notif
    hiddenWindow = window.hidden(path.join(__dirname, '../../renderer/page/notif/notif.html'));
    // Send information
    hiddenWindow.on('ready-to-show', () => {
        hiddenWindow.webContents.send('new-notif', data);
    });
}

module.exports = notif;