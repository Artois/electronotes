const {ipcMain} = require('electron');
const path = require('path');
const tray = require('./tray');
const notif = require('./notif');
const dialog = require('./dialog');
const window = require('../../../helper/window');
const demoMenu = {};

let hiddenWindow = null;

demoMenu.primeNumberPopup = function () {

};

demoMenu.primeNumber = function () {
    // demand number to the user
    dialog.custom(path.join(__dirname, '../../renderer/page/dialog/dialog.html'), number => {
        //number = 409291;
        //number = 12000000003;
        // If no answer
        if (!number) {
            return;
        }
        // Only one calc at the same time
        if (hiddenWindow) {
            dialog.error('Un calcul est déjà en cours', 'Impossible de calculer plusieurs nombre premier en même temps');
            return;
        }
        // Wait prime number is calculated
        ipcMain.once('prime-number-result', (event, result) => {
            let message = `Le nombre ${number} ` + (result ? 'est premier' : 'n\'est pas premier'); 
            dialog.message('Résultat du test de primalité', message);
            hiddenWindow.close();
            hiddenWindow = null;
        });
        //Create hidden window to calc prime number
        hiddenWindow = window.hidden(path.join(__dirname, '../../renderer/page/prime-number/prime-number.html'));
        // Send number to test when window is ready
        hiddenWindow.on('ready-to-show', () => {
            hiddenWindow.webContents.send('calc-prime-number', number);
        });
    });
};

demoMenu.primeNumberResult = function () {

};

demoMenu.tray = function () {
    // Show notif
    notif.create('Application minimisé', 'Cliquer sur l\'icone dans la barre des notifications pour réouvrir l\'application');
    // Add tray
    tray.active();
}

module.exports = demoMenu;