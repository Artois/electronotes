const { ipcMain } = require('electron');
const path = require('path');
const file = require('../../helper/file');

ipcMain.on('ondragstart', (event, data) => {
    const ext = exportToMd ? 'md' : 'json';
    const filepath = `./data/dragdrop/${data.id}/${data.title}.${ext}`;
    const content = exportToMd ? data.content : JSON.stringify(data, undefined, 4);
    // Create file
    file.makedir(filepath, true);
    file.put(filepath, content);
    // Send drag event
    event.sender.startDrag({
        file: path.join(__dirname, '../.' + filepath),
        icon: path.join(__dirname, '/asset/file.png')
    });
});
