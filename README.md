# Electronotes

Electronotes est le projet "proof of concept" sur Electron dans le cadre du module conférence de M2 ILI à l'université d'Artois par Arthur Brandao.
L'objectif de ce projet est de montrer les possibilités d'Electron dans le domaine du développement d'applications desktop.

## L'application

Electronotes est une application écrite avec les technologies web (HTML / CSS / Javascript) qui permet de créer des notes (comme [Google Keep](https://keep.google.com) par exemple).
Il est possible de créer et modifier des notes qui sont visibles sur un tableau de bord (qui est la page d'accueil).
Les notes sont écrites en Markdown avec l'aide d'un éditeur "What You See Is What You Get" (WYSIWYG).

Les notes présentes par défaut dans l'application contiennent plus d'informations sur les fonctionnalités.

## Presentation rapide d'Electron

[Electron](https://www.electronjs.org) est créé par Github pour permettre de créer des applications desktop natives sur tous les OS à partir des langages utilisés dans le web.
Il repose sur [Node.js](https://nodejs.org/en/) et [Chromium](https://www.chromium.org) pour son fonctionnement.
De nombreux logiciels de tous types (outils de développement, jeux, musique, ...) fonctionnent aujourd'hui grace Electron, en voici une liste non exhaustive :
- [Atom](https://www.electronjs.org/apps/atom)
- [Postman](https://www.electronjs.org/apps/postman)
- [Visual Studio Code](https://www.electronjs.org/apps/visual-studio-code)
- [Twitch](https://www.electronjs.org/apps/twitch)
- [Discord](https://www.electronjs.org/apps/discord)
- [Slack](https://www.electronjs.org/apps/slack)
- [Mattermost](https://www.electronjs.org/apps/mattermost)

Une liste plus complète d'applications utilisant Electron peut être trouvée sur le site dans la partie [Apps](https://www.electronjs.org/apps).

Les notes présentes par défaut dans l'application contiennent plus d'informations sur le fonctionnement d'Electron.

## Installation

Le projet a été testé sur deux environnements différents :

- KDE Neon 5.18.5 (basé sur Debian 10) avex Node.js 12.13.0 et npm 6.12.0
- Ubuntu 18.04 (basé sur Debian 10) avec Node.js 8.17.0 et npm 6.13.4

Pour installer le projet il suffit de le télécharger et de lancer la commande d'installation

```bash
npm i
# <=>
npm install
```

Les dépendances vont automatiquement être téléchargées et les fichiers SCSS utilisés dans le projet seront compilés.

Lors du premier lancement en mode desktop Electron Forge (qui est un outil facilitent le développement avec Electron) va vérifier le système d'exploitation utilisé et les dépendances natives nécessaires pour fonctionner sur l'appareil. 
Par exemple sous Windows il est nécessaire d'avoir Visual Studio C++ 2017 ou antérieur pour pouvoir lancer l'application.
Sous Linux je n'ai jamais eu besoin d'installer un logiciel ou une bibliothèque non présente, je n'ai jamais testé de lancer Electron sous Mac.

## Le projet

Le projet est prévu pour être une démonstration d'Electron et n'est en aucun cas prévu pour être distribuable ou utilisable tel quel.
Le projet a été pensé en deux parties pour montrer les possibilités d'Electron.

Les deux versions du projet possèdent des notes présentent par défaut. 
Il est fortement conseillé de les lires, elles expliquent et donnent des informations sur l'utilisation et le fonctionnement de l'application.
Les notes ayant le même nom entre les deux versions de l'application possèdent une part commune d'informations, mais on **surtout** des informations propres à leur version. 
Ainsi il est recommandé de lire toutes les notes dans les deux versions de l'application.

### Partie 1 : Application web

La première partie une application web qui fonctionne sur n'importe quel navigateur moderne si elle est mise sur un serveur HTTP. 
Il est donc possible de lancer cette version de l'application sur un serveur web ou sur Electron sans changer aucune ligne de code. 
L'objectif est de montrer qu'il est extrêment facile de transformer une application web en application desktop. 
Bien évidemment cette version de l'application n'utilise aucune fonctionalité permise par Electron puisqu'elle ne possède pas de code spécifique pour ce dernier.

Pour lancer la version web il suffit d'utiliser l'une des commandes suivantes :

```bash
# Lance la version web sur un serveur HTTP et ouvre la navigateur sur l'URL de l'application
npm run src:web

# Lance la version web de l'application avec Electron
npm run src:electron
```

Le code source de cette version est dans le dossier `src/`.

### Partie 2 : Application desktop

La seconde version de l'application est basé sur le code de la version web, mais du code spécifique à Electron et Node.js a été ajouté pour utiliser des fonctionnalités propres à ces derniers. Cette version est lanceable uniquement avec Electron et ne fonctionne pas si mis sur un serveur web.

Pour lancer la version desktop il suffit d'utiliser l'une des commandes suivantes :

```bash
# Lance la version desktop de l'application avec Electron (toutes les commandes sont équivalentes)
npm start
npm run app
npm run app:electron
```

Le code source de cette version est dans le dossier `app/`.