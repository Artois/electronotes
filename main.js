const { app, BrowserWindow } = require('electron');
const config = require('./config');
const file = require('./helper/file');
const window = require('./helper/window');
const path = require('path');
const { execSync } = require('child_process');
const { program } = require('commander');
const del = require('del');

app.allowRendererProcessReuse = true;
// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

/* --- Initialisation --- */

// Global var
global.mainWindow;
global.exportToMd;
global.trayInactive;
exportToMd = config.defaultExportToMarkdown;
trayInactive = true;
let launchInfo;
let indexFile = 'index.html';

// Add version and option
program.version('1.0.0');
program
  .option('-s, --src', 'Load the file from src folder instead of the app folder');
program.parse(process.argv);

// Check Option
const folder = program.src ? '/src/' : '/app/renderer/';

/* --- Functions --- */

// Main function
async function main() {
  // Check if launch-info.json exist
  console.log('Reading launch info');
  if (file.exist(path.join(__dirname, '/data/launch-info.json'))) {
    launchInfo = require('./data/launch-info');
    // Reset data if change source between src and app
    if (launchInfo.folder !== folder) {
      indexFile = 'reset.html';
      launchInfo.export = config.defaultExportToMarkdown ? 'md' : 'json';
    }
    // Update launch info
    launchInfo.folder = folder
    // Read export method
    console.log(launchInfo.export);
    exportToMd = launchInfo.export === 'md';
    console.log(exportToMd);
  } else {
    // Create default launch info
    launchInfo = {
      folder: folder,
      export: exportToMd ? 'md' : 'json'
    }
  }
  // if launch the advanced electron app
  if (!program.src) {
    // Loading main process files
    loadMainProcessFiles();
    // Compile SCSS file from app
    console.info('Compiling SCSS files in CSS');
    let scssFile = path.join(__dirname, folder, 'common/css/style.scss');
    let cssFile = path.join(__dirname, folder, 'common/css/style.min.css');
    execSync(`node-sass --output-style compressed ${scssFile} ${cssFile}`);
    scssFile = path.join(__dirname, folder, 'common/css/materialize/materialize.scss');
    cssFile = path.join(__dirname, folder, 'common/css/materialize.min.css');
    execSync(`node-sass --output-style compressed ${scssFile} ${cssFile}`);
  }
  // Load main window
  console.info('Load main window');
  createMainWindow(program.src);
}

// Create main window function
function createMainWindow(simple = false) {
  if (simple) {
    mainWindow = window.simple(path.join(__dirname, folder, indexFile));
  } else {
    mainWindow = window.new(path.join(__dirname, folder, indexFile));
  }
  mainWindow.on('close', () => {
    mainWindow = null;
  });
}

function loadMainProcessFiles() {
  const mainProcessFiles = file.list(path.join(__dirname, '/app/main/'));
  mainProcessFiles.forEach(f => {
    const filepath = path.join(__dirname, '/app/main/', f)
    if (file.isFile(filepath)) {
      require(filepath);
    }
  });
}

/* --- Electron app actions --- */

// This method will be called when Electron is ready
app.on('ready', main);
// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  // The application don't quit if the tray is active
  if (process.platform !== 'darwin' && trayInactive) {
    app.quit();
  }
});
// Re-open
app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createMainWindow(program.src);
  }
});
// Application stop
app.on('quit', () => {
  // Write launch info file
  launchInfo.export = exportToMd ? 'md' : 'json';
  file.put(path.join(__dirname, '/data/launch-info.json'), JSON.stringify(launchInfo));
  // Delete export files 
  if(file.exist('./data/dragdrop/')) {
    console.log('Deleting all exported files')
    del.sync(['./data/dragdrop/**', './data/dragdrop']);
  }
});
 
